# Expensify

## Description
The project “Expensify” is an idea to implement an expense tracking application by using
XCode. Money and finances are something we deal with every day in our lives and an app to
keep track of the expenses would allow people to categorize spendings, get an overall look at
where the money is being spent, and be able to create a budget accordingly. This project will be
able to use the current date and location to create more accurate records of where the
spendings occurred. I will also add functionality of adding a picture with the expense either
using the camera or through the library.

## Design & Implementation
The project will have 3 UIViewControllers. I will use Tabbar navigator to switch between these
views.
### View Controllers:
1. HomeViewController: showing current balance, total income, total expenses, and a list
of recent expenses.
2. AddViewController: will display a list of categories for expenses that the users can
choose to add. After adding an expense automatically go back to the homepage.
3. AllViewController: will display a all the expenses that were made. 
